import argparse
import logging
import math
from collections import Counter

import requests

KNOWN_COLORS = ["white", "yellow", "blue", "purple"]

API_GET_USER_INFO = "https://www.codewars.com/api/v1/users/{username}"
API_GET_COMPLETED_CHALLENGES = "https://www.codewars.com/api/v1/users/{username}/code-challenges/completed"
API_GET_CHALLENGE_INFO = "https://www.codewars.com/api/v1/code-challenges/{id}"


def is_api_error(obj: dict) -> bool:
    return not obj.get("success", True)


def get_all_pages(url) -> list:
    req_json = requests.get(url).json()

    data = req_json.get("data", None)
    if is_api_error(req_json) or data is None:
        raise (ValueError(req_json.get("reason", "Unknown error")))

    total_pages = req_json.get("totalPages", 1)

    if total_pages > 1:
        raise NotImplementedError('should implement support for more then 1 page')

    return data


def get_user_info(username: str) -> dict:
    req_json = requests.get(API_GET_USER_INFO.format(username=username)).json()

    if is_api_error(req_json):
        raise ValueError(req_json.get("reason"))

    return req_json


def get_user_languages(username: str) -> list:
    return list(get_user_info(username)["ranks"]["languages"].keys())


def get_all_solved_challenges(username: str, language: str) -> list:
    return list(
        filter(
            lambda challenge: language.lower() in challenge.get("completedLanguages", []),
            get_all_pages(API_GET_COMPLETED_CHALLENGES.format(username=username)),
        )
    )


def get_challenge_info(challenge_id: str) -> dict:
    req_json = requests.get(API_GET_CHALLENGE_INFO.format(id=challenge_id)).json()

    if is_api_error(req_json):
        raise ValueError(req_json.get("reason"))

    return req_json


def get_challenge_color(challenge_id: str) -> str:
    return get_challenge_info(challenge_id)["rank"]["color"]


def calc_points(white=0, yellow=0, blue=0, purple=0) -> float:
    # (logbase2(white) + 1) + (logbase1.5(yellow) + 1) + 3 * blue + 4 * purple

    a = (math.log(white, 2) + 1) if white > 0 else 0
    b = (math.log(yellow, 1.5) + 1) if yellow > 0 else 0
    c = 3 * blue
    d = 4 * purple

    return a + b + c + d


def calc_points_for_lang(username: str, lang: str) -> float:
    challenges_color = []
    for c in get_all_solved_challenges(username, lang):
        try:
            color = get_challenge_color(c["id"])
            if color not in KNOWN_COLORS:
                logging.warning("Challenge with id {} on language {} have unknown color '{}', skiping".format(c["id"], lang, color))
            else:
                challenges_color.append(color)
        except ValueError:
            logging.warning("challenge with id {} on language {} not found, skiping".format(c["id"], lang))
    return calc_points(**Counter(challenges_color))


def main():

    parser = argparse.ArgumentParser(description="Calculate score for codewars on beta.")
    parser.add_argument("username", help="Username on codewars")
    parser.add_argument(
        "--languages",
        "-l",
        metavar="LANG",
        type=str,
        nargs="+",
        help="A languages to calculate score on. if not used, using all user languages",
    )
    args = parser.parse_args()
    username = args.username
    languages = args.languages

    if languages is None:
        languages = get_user_languages(username)

    for lang in languages:
        points = calc_points_for_lang(username, lang)
        print(lang, "-", points)


if __name__ == "__main__":
    main()
