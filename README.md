# codewars-calc

Calculate the points a user should be reworded from https://codewars.com/ site to beta table

## Usage

```sh
usage: codewars-calc.py [-h] [--languages LANG [LANG ...]] username

Calculate score for codewars on beta.

positional arguments:
  username              Username on codewars

optional arguments:
  -h, --help            show this help message and exit
  --languages LANG [LANG ...], -l LANG [LANG ...]
                        A languages to calculate score on. if not used, using
                        all user languages
```
