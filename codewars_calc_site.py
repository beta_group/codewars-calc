from datetime import datetime, timedelta
from functools import lru_cache, wraps

from flask import Flask

from codewars_calc import calc_points_for_lang, get_user_info

app = Flask(__name__)


def timed_lru_cache(minutes: int, maxsize: int = 128):

    def wrapper_cache(func):
        func = lru_cache(maxsize=maxsize)(func)
        func.lifetime = timedelta(minutes=minutes)
        func.expiration = datetime.utcnow() + func.lifetime

        @wraps(func)
        def wrapped_func(*args, **kwargs):
            if datetime.utcnow() >= func.expiration:
                func.cache_clear()
                func.expiration = datetime.utcnow() + func.lifetime

            return func(*args, **kwargs)

        return wrapped_func

    return wrapper_cache


@timed_lru_cache(minutes=15)
def calc_points_for_lang_cached(username: str, language: str) -> float:
    return calc_points_for_lang(username, language)


@timed_lru_cache(minutes=15)
def get_user_info_cached(username: str) -> dict:
    user_info = get_user_info(username)

    ranks = user_info.get('ranks', {})

    languages = ranks.get('languages', {})
    languages = {lang: languages[lang]['color'] for lang in languages.keys()}

    overall = ranks.get('overall', {})

    return {
        'username': user_info.get('username'),
        'languages': languages,
        'clan': user_info.get('clan', 'no clan'),
        'color': overall.get('color')
    }


@app.route('/score/<username>/<language>')
def score(username: str, language: str):
    score = calc_points_for_lang_cached(username, language)
    return {
        'username': username,
        'language': language,
        'score': score,
        'rounded_score': round(score),
    }


@app.route('/user/<username>')
def user(username: str):
    return get_user_info_cached(username)


@app.route("/")
def index():
    return """routes:<br>
    /score/&lt;username&gt;/&lt;language&gt; - to get points of user<br>
    /user/&lt;username&gt; - to get info of user"""
